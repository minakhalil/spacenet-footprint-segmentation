## Overview
The task of SpaceNet 6 is to automatically extract building footprints from SAR (4 bands) images. Synthetic Aperture Radar (SAR) is a unique form of radar that can penetrate clouds, collect during all- weather conditions, and capture data day and night.
*further details*: https://spacenet.ai/sn6-challenge/

## Dataset
The Data – Over 120 sq km of both high resolution synthetic aperture radar (SAR) data and electro optical (EO) imagery with ~48,000 building footprint labels of Rotterdam, The Netherlands

**Training Data (39.0 GB)**
```
$ aws s3 cp s3://spacenet-dataset/spacenet/SN6_buildings/tarballs/SN6_buildings_AOI_11_Rotterdam_train.tar.gz .
```

**Testing Data (17.0 GB)**
```
$ aws s3 cp s3://spacenet-dataset/spacenet/SN6_buildings/tarballs/SN6_buildings_AOI_11_Rotterdam_test_public.tar.gz .
```

## Results
Achieved **F1-Score = 0.48** on the Validation Set<br /> *[Polygon Wise - IOU with threshold 0.5]*

![](images/1.png)
![](images/2.png)
![](images/3.png)
![](images/4.png)

## Solution
### Proposed architecture 
- Simple Unet *[5 levels]*.
- PreTrained VGG19 with BN used as encoder (Backbone/Feature extractor).
- Followed by 5 UpSampling layers as a decoder.
- BCE pixel loss.

### Ideas
`[✕] means tried but doesn't help`
- [√] Split large images to a small patches in order to have ability to use large batch size.
- [√] Transform SAR 4 channels (HH,HV,VH,VV) to RGB using Pauli formula.
- [√] Use only scaling augmentations to keep SAR images valid.
- [✕] Add adversarial loss for refining the output mask.
- [✕] Estimate the optical image using a Compressor network and L1-Loss.



## LIBs [Python3.6]
```
Pytorch 1.4.0
Albumentations 0.4.5
Rasterio 1.1.3
Torchsummary 1.5.1
Opencv_python 4.2.0.32
Numpy 1.17.2
Pandas 0.25.1
Shapely 1.7.0
Torchvision 0.5.0
```

## Run
[1] Place dataset folders.<br />
[2] Check `config.py` file.<br />
[3]
```
$ pip install requirements.txt
$ python3.6 train.py
$ python3.6 predict.py
```
## References/Papers
[1] https://arxiv.org/abs/1505.04597<br />
