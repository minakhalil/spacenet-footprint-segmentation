import os
import time
from tqdm import tqdm

from .dataset import SpaceNetTestDataset
from torch.utils.data import DataLoader
from .config import cnf
import torch
from .networks import UnetV2
from .utils import extract_polys
from shapely.wkt import loads
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from torchsummary import summary

# check for GPU
use_cuda = torch.cuda.is_available() and cnf["USE_GPU"]
device = torch.device("cuda:0" if use_cuda else "cpu")


def predict(epoch_to_restore=""):
    global USE_GAN

    if os.path.exists(cnf["SUBMISSION_FILE"]):
        os.remove(cnf["SUBMISSION_FILE"])

    # load test set
    print("\nLoading Dataset...")
    test_dataset = SpaceNetTestDataset()
    test_dataloader = DataLoader(test_dataset, batch_size=1, num_workers=cnf['N_WORKERS'])

    print("\nLoad Model...\n")
    if not os.path.exists(cnf["CHECKPOINTS_DIR"]) or len(os.listdir(cnf["CHECKPOINTS_DIR"])) == 0:
        exit("checkpoint not found")

    if epoch_to_restore != "" and not os.path.exists(os.path.join(cnf["CHECKPOINTS_DIR"], "CK-" + epoch_to_restore)):
        exit("epoch not found")

    if epoch_to_restore == "":
        # load from the most recent checkpoint
        files = os.listdir(cnf["CHECKPOINTS_DIR"])
        paths = [os.path.join(cnf["CHECKPOINTS_DIR"], basename) for basename in files if "CK-" in basename]
        checkpoint = max(paths, key=os.path.getctime)
    else:
        # load from a specific checkpoint
        checkpoint = os.path.join(cnf["CHECKPOINTS_DIR"], "CK-" + epoch_to_restore)

    print("Load From:", checkpoint)
    state = torch.load(checkpoint)

    # construct model architecture
    unet_model = UnetV2(in_channels=3, out_classes=1)
    unet_model.to(device=device)
    unet_model.load_state_dict(state['unet'])
    unet_model.eval()

    time.sleep(1)

    for bt_idx, (x_img_, img_id, img_orientation) in enumerate(tqdm(test_dataloader, desc="Predicting")):

        # load images to GPU
        x_images = x_img_.to(device=device, dtype=torch.float32)

        # predict
        pred_masks = unet_model(x_images)
        pred_masks = torch.sigmoid(pred_masks).cpu()

        # convert mask to be binary [0,255]
        pred_masks_binary = (pred_masks > 0.5) * 255

        # sigmoid output mask for Confidence calculations
        pred_masks = pred_masks.data.numpy()
        pred_masks = np.squeeze(pred_masks.astype(np.uint8), axis=1)

        pred_masks_binary = pred_masks_binary.data.numpy()
        pred_masks_binary = np.squeeze(pred_masks_binary.astype(np.uint8), axis=1)

        for mask_id, mask in enumerate(pred_masks):
            polys, conf = extract_polys(pred_masks_binary[mask_id], img_orientation=img_orientation[mask_id],
                                        sigmoid_mask=pred_masks[mask_id], simplify=True,
                                        min_area=80.0)
            image_id = [img_id[mask_id] for _ in conf]

            # append to the submission file
            submission_file = open(cnf["SUBMISSION_FILE"], "a")

            if len(polys) > 0:
                # data frame contain all polygons for an image_id
                df = pd.DataFrame({"ImageId": image_id, "PolygonWKT_Pix": polys, "Confidence": conf})
            else:
                # if not founds any footprints
                df = pd.DataFrame(
                    {"ImageId": img_id[mask_id], "PolygonWKT_Pix": ["POLYGON EMPTY"], "Confidence": [1.0]})

            df.to_csv(submission_file, header=submission_file.tell() == 0, index=False)
            submission_file.close()

    print("\nFinish")


with torch.no_grad():
    predict(epoch_to_restore="")
