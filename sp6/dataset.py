import random
import torch
from torch.utils import data
from tqdm import tqdm
from .config import cnf
import pandas as pd
import numpy as np
import os
import cv2
from .utils import read_sar_tiff_image, construct_mask_image, transform_sar
from albumentations import ShiftScaleRotate, Compose
import shutil


def filter_non_existing_images(image_id):
    path_sar = os.path.join(cnf["SAR_DIR"], cnf["SAR_IMAGE_PREFIX"] + image_id + ".tif")
    return os.path.exists(path_sar)


class SpaceNetTrainDataset(data.Dataset):

    def __init__(self, reconstruct_tmp=False):

        # read dataframe
        df = pd.read_csv(cnf["SUMMARY_DIR"])
        self.data_frame = df.groupby('ImageId')

        # read SAR orientations
        self.orientations_df = pd.read_csv(cnf["ORIENTATION_DIR"], delimiter=' ', header=None, index_col=False)
        self.orientations_df.set_index(0, inplace=True)

        # filter non-existing images
        init_image_ids = list(self.data_frame.groups.keys())
        image_ids = list(filter(filter_non_existing_images, init_image_ids))

        # only applicable augmentation to be applies to SAR is scaling
        self.augmentation = Compose([
            ShiftScaleRotate(shift_limit=0, scale_limit=0.5, rotate_limit=0, interpolation=1,
                             border_mode=cv2.BORDER_CONSTANT, value=[0, 0, 0, 0], mask_value=0),

        ], p=1.0)

        # shuffle the total ids
        random.seed(cnf["SEED"])
        random.shuffle(image_ids)

        # split train/validation
        val_len = int(cnf["VALIDATION_RATIO"] * len(image_ids))

        self.image_ids = image_ids[val_len:]

        if reconstruct_tmp:
            self.image_ids = self.pretrain_trainset_construction(image_ids=self.image_ids)
        else:
            self.image_ids = [obj.name.replace(".npy", "") for obj in os.scandir(cnf["TEMP_DIR"]) if obj.is_file()]

        random.seed(cnf["SEED"])
        random.shuffle(self.image_ids)

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, idx):

        img = np.load(os.path.join(cnf["TEMP_DIR"], self.image_ids[idx] + '.npy'))

        processed_sar = transform_sar(img[3:-1])
        x_img_rgb = torch.Tensor(processed_sar)

        # x_img_rgb = torch.Tensor(img[0:3])
        x_img_sar = torch.Tensor(img[3:-1])
        y_img_mask = torch.Tensor(img[-1:])

        return x_img_rgb, x_img_sar, y_img_mask

    def pretrain_trainset_construction(self, image_ids):
        '''
        - Read the image band
        - Apply rotation if needed
        - Construct Augmented images
        - For each image (900*900) create a 16 small image each (225,225)
        - Save each small image in a .npy file in TEMP folder

        :param image_ids: the original image ids
        :return: new image ids (original_image_id#slice_id-#version_id)
        '''
        # remove old temp directory
        if os.path.exists(cnf["TEMP_DIR"]):
            shutil.rmtree(cnf["TEMP_DIR"])

        # create new directory
        os.mkdir(cnf["TEMP_DIR"])

        new_ids = []
        for img_id in tqdm(image_ids, desc="Create TMP Traindata"):
            # read sar image
            x_img_sar_path = os.path.join(cnf["SAR_DIR"], cnf["SAR_IMAGE_PREFIX"] + img_id + ".tif")
            x_img_sar = read_sar_tiff_image(x_img_sar_path)

            x_img_size = x_img_sar.shape[1]

            # construct ground truth mask
            y_img_mask, y_polys_str = construct_mask_image(self.data_frame, img_id, x_img_size)
            y_img_mask = np.expand_dims(y_img_mask, axis=0)

            # versions are the original image and the augmented ones
            versions_per_image = 1 + cnf["AUGMENTATION_FACTOR"] if cnf["USE_AUGMENTATION"] else 1

            for version in range(versions_per_image):
                # augmentation only applies for versions > 0
                if cnf["USE_AUGMENTATION"] and version != 0:
                    x_img = x_img.transpose([1, 2, 0])
                    y_img_mask = y_img_mask.transpose([1, 2, 0])

                    data = {"image": x_img, "mask": y_img_mask}
                    augmented = self.augmentation(**data)

                    x_img, y_img_mask = augmented["image"], augmented["mask"]

                    x_img = x_img.transpose([2, 0, 1])
                    y_img_mask = y_img_mask.transpose([2, 0, 1])

                # concat the 4 channels sar with 1 channel mask in one array
                concat_img = np.concatenate([x_img, y_img_mask], axis=0)

                # rotate to fit orientation
                timestamp = img_id.split("_tile_")[0]
                img_orientation = self.orientations_df.loc[timestamp][1]

                if img_orientation == 1:
                    concat_img = np.flip(concat_img, axis=(1, 2))

                concat_img_tensor = torch.Tensor(np.expand_dims(concat_img, axis=0).copy())

                # create the small splits
                slices, _ = slicing_single_batch(concat_img_tensor, orignal_width=concat_img.shape[1],
                                                 channels=concat_img.shape[0])

                # save each split in a separate (.npy) file
                for slice_i in range(cnf["SINGLE_IMAGE_SPLITS"]):
                    array_to_write = slices[slice_i].data.numpy()

                    if not array_to_write.any():  # if all zeros (empty image) -> skip
                        continue

                    file_name = img_id + "#" + str(slice_i) + "-" + str(version)
                    new_ids.append(file_name)

                    np.save(os.path.join(cnf["TEMP_DIR"], file_name), array_to_write)

        return new_ids


class SpaceNetValidationDataset(data.Dataset):

    def __init__(self):
        # read dataframe
        df = pd.read_csv(cnf["SUMMARY_DIR"])
        self.data_frame = df.groupby('ImageId')

        # read SAR orientations
        self.orientations_df = pd.read_csv(cnf["ORIENTATION_DIR"], delimiter=' ', header=None, index_col=False)
        self.orientations_df.set_index(0, inplace=True)

        init_image_ids = list(self.data_frame.groups.keys())
        image_ids = list(filter(filter_non_existing_images, init_image_ids))

        # shuffle the total ids
        random.seed(cnf["SEED"])
        random.shuffle(image_ids)

        # split train/validation
        val_len = int(cnf["VALIDATION_RATIO"] * len(image_ids))

        self.image_ids = image_ids[:val_len]

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, idx):
        # read the train image values
        image_id = self.image_ids[idx]

        x_img_sar_path = os.path.join(cnf["SAR_DIR"], cnf["SAR_IMAGE_PREFIX"] + image_id + ".tif")
        x_img_sar = read_sar_tiff_image(x_img_sar_path)

        # pauli transformed image
        x_img_rgb = transform_sar(x_img_sar)

        x_img_size = x_img_sar.shape[1]

        # construct ground truth mask
        y_img_mask, y_polys_str = construct_mask_image(self.data_frame, image_id, x_img_size)
        y_img_mask = np.expand_dims(y_img_mask, axis=0)

        # rotate to fit orientation (if specified)
        timestamp = image_id.split("_tile_")[0]
        img_orientation = self.orientations_df.loc[timestamp][1]

        if img_orientation == 1:
            x_img_sar = np.flip(x_img_sar, axis=(1, 2)).copy()
            x_img_rgb = np.flip(x_img_rgb, axis=(1, 2)).copy()
            y_img_mask = np.flip(y_img_mask, axis=(1, 2)).copy()

        return x_img_rgb, x_img_sar, y_img_mask, y_polys_str, img_orientation


def slicing_single_batch(x_img, orignal_width=900, channels=4):
    '''
    Slice the large image to a small images with the same size
    :param x_img: Original large image
    :param orignal_width: width = height
    :param channels:
    :return: small patches and the size
    '''
    row_splits = cnf["SINGLE_IMAGE_SPLITS"] ** (1 / 2)
    assert orignal_width % row_splits == 0, "Row width should be divisible by original width"

    grid_size = int(orignal_width // row_splits)

    patches = x_img.unfold(1, channels, channels).unfold(2, grid_size, grid_size).unfold(3, grid_size,
                                                                                         grid_size)
    unfold_shape = patches.size()
    patches = patches.contiguous().view(-1, channels, grid_size, grid_size)
    return patches, unfold_shape


def merge_sliced_batch(patches, unfold_shape):
    '''
    Merge all sliced small patches to construct the large original image
    :param patches:
    :param unfold_shape:
    :return:
    '''
    # patches = patches.unsqueeze(0)
    patches_orig = patches.view(unfold_shape)
    output_c = unfold_shape[1] * unfold_shape[4]
    output_h = unfold_shape[2] * unfold_shape[5]
    output_w = unfold_shape[3] * unfold_shape[6]
    patches_orig = patches_orig.permute(0, 1, 4, 2, 5, 3, 6).contiguous()
    patches_orig = patches_orig.view(unfold_shape[0], output_c, output_h, output_w)

    return patches_orig


class SpaceNetTestDataset(data.Dataset):

    def __init__(self):
        # read SAR orientations
        self.orientations_df = pd.read_csv(cnf["ORIENTATION_DIR"], delimiter=' ', header=None, index_col=False)
        self.orientations_df.set_index(0, inplace=True)

        # explore the Test directory and get images ids
        self.test_images = [obj.name for obj in os.scandir(cnf["SAR_DIR_TEST"]) if obj.is_file()]

    def __len__(self):
        return len(self.test_images)

    def __getitem__(self, idx):
        #read image
        image_path = os.path.join(cnf["SAR_DIR_TEST"], self.test_images[idx])
        image_id = self.test_images[idx].replace(cnf["SAR_IMAGE_PREFIX_TEST"], "").replace(".tif", "")

        #apply Pauli transform
        x_img = read_sar_tiff_image(image_path)
        x_img = transform_sar(x_img)

        # rotate to fit orientation
        timestamp = image_id.split("_tile_")[0]
        img_orientation = self.orientations_df.loc[timestamp][1]

        if img_orientation == 1:
            x_img = np.flip(x_img, axis=(1, 2)).copy()

        return x_img, image_id, img_orientation
