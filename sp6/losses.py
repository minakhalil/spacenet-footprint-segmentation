from torch import nn

'''
Calculate total losses
'''


def unet_loss():
    return nn.BCEWithLogitsLoss()

def pixel_loss():
    return nn.L1Loss()

def generator_loss():
    return nn.BCEWithLogitsLoss()

def criterionGAN():
    return nn.BCELoss()

