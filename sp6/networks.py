import torch
from torch import nn
from torchvision import models
from .config import cnf


class DownBlock(nn.Module):

    def __init__(self, in_channels, out_channels, with_max=True, conv_lvls=2):
        super().__init__()

        self.with_max = with_max

        self.conv_1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
        self.bn_1 = nn.BatchNorm2d(out_channels)
        self.relu_1 = nn.ReLU()
        self.layers = nn.ModuleList()

        for i in range(conv_lvls - 1):
            self.layers += [
                nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1),
                nn.BatchNorm2d(out_channels),
                nn.ReLU()]

        self.max_p = nn.MaxPool2d(2)

    def forward(self, x):
        x = self.conv_1(x)
        x = self.bn_1(x)
        x = self.relu_1(x)

        for layer in self.layers:
            x = layer(x)

        x_max_p = self.max_p(x)

        return x, x_max_p


class UpBlock(nn.Module):

    def __init__(self, in_channels, out_channels, same=False):
        super().__init__()

        self.up = nn.ConvTranspose2d(in_channels, out_channels, kernel_size=3, stride=2, output_padding=1, padding=1)

        if same:
            in_channels *= 2
        self.conv_1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
        self.bn_1 = nn.BatchNorm2d(out_channels)
        self.relu_1 = nn.ReLU()
        self.conv_2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1)
        self.bn_2 = nn.BatchNorm2d(out_channels)
        self.relu_2 = nn.ReLU()

    def forward(self, x, res):
        x = self.up(x)
        x_width, x_height = x.size()[3], x.size()[2]
        res_width, res_height = res.size()[3], res.size()[2]

        x_center_w, x_center_h = x_width // 2, x_height // 2
        res_center_w, res_center_h = res_width // 2, res_height // 2

        res = res[:, :, res_center_h - x_center_h:res_center_h + x_center_h,
              res_center_w - x_center_w:res_center_w + x_center_w]

        x = torch.cat([res, x], dim=1)

        x = self.conv_1(x)
        x = self.bn_1(x)
        x = self.relu_1(x)
        x = self.conv_2(x)
        x = self.bn_2(x)
        x = self.relu_2(x)

        return x


class UnetV2(nn.Module):

    def __init__(self, in_channels, out_classes):
        super().__init__()

        self.conv_1 = nn.Conv2d(in_channels, 3, kernel_size=3, padding=1)
        self.bn_1 = nn.BatchNorm2d(3)
        self.relu_1 = nn.ReLU()

        self.conv_2 = nn.Conv2d(3, 3, kernel_size=3, padding=1)
        self.bn_2 = nn.BatchNorm2d(3)
        self.relu_2 = nn.ReLU()

        self.vgg19 = models.vgg19_bn(pretrained=False)
        self.vgg19.load_state_dict(torch.load(cnf["vgg19_pretrained"]))

        self.desired_res_layers = [5, 12, 25, 38, 51]

        self.vgg19_features = list(self.vgg19.features)
        self.vgg19 = nn.Sequential(*self.vgg19_features)

        self.bottle_neck = DownBlock(512, 1024, conv_lvls=4)

        self.up_1 = UpBlock(1024, 512)
        self.up_2 = UpBlock(512, 512, same=True)
        self.up_3 = UpBlock(512, 256)
        self.up_4 = UpBlock(256, 128)
        self.up_5 = UpBlock(128, 64)

        self.out = nn.Conv2d(64, out_classes, kernel_size=2, padding=1)

    def forward(self, x):

        orig_height, orig_width = x.size()[2], x.size()[3]

        x = self.conv_1(x)
        x = self.bn_1(x)
        x = self.relu_1(x)

        x = self.conv_2(x)
        x = self.bn_2(x)
        x = self.relu_2(x)

        res_outputs = []

        for i_layer, layer in enumerate(self.vgg19_features):
            x = layer(x)

            if i_layer in self.desired_res_layers:
                res_outputs.append(x)

        x, _ = self.bottle_neck(x)

        x = self.up_1(x, res_outputs[-1])
        x = self.up_2(x, res_outputs[-2])
        x = self.up_3(x, res_outputs[-3])
        x = self.up_4(x, res_outputs[-4])
        x = self.up_5(x, res_outputs[-5])

        x = self.out(x)

        x = nn.functional.interpolate(x.float(), size=(orig_height, orig_width))

        return x


        
class CompressorV2(nn.Module):

    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.conv_1 = nn.Conv2d(in_channels, 64, kernel_size=3, padding=1)
        self.relu_1 = nn.LeakyReLU(0.2, True)

        self.conv_2 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.bn_2 = nn.BatchNorm2d(64)
        self.relu_2 = nn.LeakyReLU(0.2, True)

        self.conv_3 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.bn_3 = nn.BatchNorm2d(32)
        self.relu_3 = nn.LeakyReLU(0.2, True)

        self.conv_4 = nn.Conv2d(32, 16, kernel_size=3, padding=1)
        self.bn_4 = nn.BatchNorm2d(16)
        self.relu_4 = nn.LeakyReLU(0.2, True)

        self.conv_1_rev = nn.Conv2d(16, 32, kernel_size=3, padding=1)
        self.bn_1_rev = nn.BatchNorm2d(32)
        self.relu_1_rev = nn.LeakyReLU(0.2, True)

        self.conv_2_rev = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.bn_2_rev = nn.BatchNorm2d(64)
        self.relu_2_rev = nn.LeakyReLU(0.2, True)

        self.conv_3_rev = nn.Conv2d(128, 64, kernel_size=3, padding=1)
        self.bn_3_rev = nn.BatchNorm2d(64)
        self.relu_3_rev = nn.LeakyReLU(0.2, True)

        self.conv_4_rev = nn.Conv2d(128, out_channels, kernel_size=3, padding=1)

        self.out = nn.Sigmoid()

    def forward(self, x):
        x = self.conv_1(x)
        x = res_1 = self.relu_1(x)

        x = self.conv_2(x)
        x = self.bn_2(x)
        x = res_2 = self.relu_2(x)

        x = self.conv_3(x)
        x = self.bn_3(x)
        x = res_3 = self.relu_3(x)

        x = self.conv_4(x)
        x = self.bn_4(x)
        x = self.relu_4(x)

        x = self.conv_1_rev(x)
        x = self.bn_1_rev(x)
        x = self.relu_1_rev(x)

        cat_1 = torch.cat([res_3, x], dim=1)

        x = self.conv_2_rev(cat_1)
        x = self.bn_2_rev(x)
        x = self.relu_2_rev(x)

        cat_2 = torch.cat([res_2, x], dim=1)

        x = self.conv_3_rev(cat_2)
        x = self.bn_3_rev(x)
        x = self.relu_3_rev(x)

        cat_3 = torch.cat([res_1, x], dim=1)

        x = self.conv_4_rev(cat_3)
        # x = self.out(x)

        return x

class GAN_Generator(nn.Module):
    '''
    Used for refining output mask
    '''

    def __init__(self, out_classes):
        super().__init__()

        self.conv_1 = nn.Conv2d(out_classes, out_classes, kernel_size=5, padding=2, stride=1)
        self.bn_1 = nn.BatchNorm2d(out_classes)
        self.relu_1 = nn.LeakyReLU(0.2, True)

        self.conv_2 = nn.Conv2d(out_classes, out_classes, kernel_size=5, padding=2, stride=1)
        self.bn_2 = nn.BatchNorm2d(out_classes)
        self.relu_2 = nn.LeakyReLU(0.2, True)

        self.conv_3 = nn.Conv2d(out_classes, out_classes, kernel_size=5, padding=2, stride=1)

    def forward(self, x):
        x = self.conv_1(x)
        x = self.bn_1(x)
        x = self.relu_1(x)

        x = self.conv_2(x)
        x = self.bn_2(x)
        x = self.relu_2(x)

        x = self.conv_3(x)

        return x

class GAN_Discriminator(nn.Module):

    def __init__(self, in_channels):
        super().__init__()

        self.conv_1 = nn.Conv2d(in_channels, 64, kernel_size=4, padding=1, stride=2)
        self.l_relu_1 = nn.LeakyReLU(0.2, True)

        self.conv_2 = nn.Conv2d(64, 128, kernel_size=4, padding=1, stride=2)
        self.bn_2 = nn.BatchNorm2d(128)
        self.l_relu_2 = nn.LeakyReLU(0.2, True)

        self.conv_3 = nn.Conv2d(128, 256, kernel_size=4, padding=1, stride=2)
        self.bn_3 = nn.BatchNorm2d(256)
        self.l_relu_3 = nn.LeakyReLU(0.2, True)

        self.conv_4 = nn.Conv2d(256, 512, kernel_size=4, padding=1, stride=2)
        self.bn_4 = nn.BatchNorm2d(512)
        self.l_relu_4 = nn.LeakyReLU(0.2, True)

        self.conv_5 = nn.Conv2d(512, 1, kernel_size=4, padding=1, stride=1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, orig, mask):
        x = torch.cat([orig, mask], dim=1)
        x = self.conv_1(x)
        x = self.l_relu_1(x)

        x = self.conv_2(x)
        x = self.bn_2(x)
        x = self.l_relu_2(x)

        x = self.conv_3(x)
        x = self.bn_3(x)
        x = self.l_relu_3(x)

        x = self.conv_4(x)
        x = self.bn_4(x)
        x = self.l_relu_4(x)

        x = self.conv_5(x)
        x = self.sigmoid(x)

        return x