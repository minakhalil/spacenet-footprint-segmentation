import os
import time
from torch import nn, optim
from tqdm import tqdm
from .dataset import SpaceNetTrainDataset, SpaceNetValidationDataset
from torch.utils.data import DataLoader
from .config import cnf
import torch
from .losses import unet_loss
from .networks import UnetV2
from .utils import calc_scores, calc_f1, extract_polys
from shapely.wkt import loads
import numpy as np
from matplotlib import pyplot as plt
from torchsummary import summary

# check for GPU
use_cuda = torch.cuda.is_available() and cnf["USE_GPU"]
device = torch.device("cuda:0" if use_cuda else "cpu")

# used to only run validation set
VALIDATION_ONLY = cnf['VALIDATION_ONLY']


def train_model(restore_epoch_number=""):
    print("\nRUN using:", device)
    print("Batch Size:", cnf["BATCH_SZ"])

    global VALIDATION_ONLY

    # unet
    unet_model = UnetV2(in_channels=3, out_classes=1)
    unet_model.to(device=device)

    # losses
    unet_l = unet_loss().to(device=device)

    # optimizers
    adam_optimizer_unet = optim.Adam(
        filter(lambda p: p.requires_grad, unet_model.parameters()),
        lr=cnf["LEARNING_RATE"])

    # lr scheduler for LR decay
    lr_scheduler_unet = optim.lr_scheduler.ReduceLROnPlateau(adam_optimizer_unet, mode="min",
                                                             patience=cnf["SCHEDULER_PATIENCE"])

    # create checkpoint DIR if not exists
    if not os.path.exists(cnf["CHECKPOINTS_DIR"]):
        os.mkdir(cnf["CHECKPOINTS_DIR"])

    # load last saved if exists
    files = os.listdir(cnf["CHECKPOINTS_DIR"])
    paths = [os.path.join(cnf["CHECKPOINTS_DIR"], basename) for basename in files if "CK-" in basename]
    is_loading = True if len(paths) > 0 else False

    if is_loading:  # find a valid checkpoint

        # if need to continue from a specific checkpoint
        if restore_epoch_number == "":
            newest_checkpoint = max(paths, key=os.path.getctime)
            state = torch.load(newest_checkpoint)
        else:
            state = torch.load(cnf["CHECKPOINTS_DIR"] + "/CK-" + restore_epoch_number)

        start_epoch = state['epoch'] + 1
        print("\nLoading Model :", start_epoch - 1)

        # restore states
        unet_model.load_state_dict(state['unet'])
        lr_scheduler_unet.load_state_dict(state['scheduler_unet'])
        adam_optimizer_unet.load_state_dict(state['optimizer_unet'])

    else:
        start_epoch = 0

    print("\nLOADING DATASETS...")

    # load dataset
    train_set = SpaceNetTrainDataset(reconstruct_tmp=not is_loading)
    validation_set = SpaceNetValidationDataset()

    # data loaders
    train_dataloader = DataLoader(train_set, batch_size=cnf["BATCH_SZ"], shuffle=True, num_workers=cnf['N_WORKERS'])
    val_dataloader = DataLoader(validation_set, batch_size=1, shuffle=False, num_workers=cnf['N_WORKERS'])

    for epoch in range(start_epoch, cnf["EPOCHS"]):

        '''
        TRAINGING LOOP
        '''

        if not VALIDATION_ONLY:
            print("\n\n#####################")
            print("EPOCH:", epoch, "/" + str(cnf["EPOCHS"]) + "\n")

        total_loss_unet, total_loss_gen, total_loss_disc = 0, 0, 0
        TP, FP, FN = 0, 0, 0
        f1_total = []

        if not VALIDATION_ONLY:

            unet_model.train()
            adam_optimizer_unet.zero_grad()

            time.sleep(1)

            for batch_idx, (x_img_rgb_, x_img_sar_, y_img_mask_) in enumerate(tqdm(train_dataloader, desc="Training")):

                # fig=plt.figure()
                # i1 = y_img_mask[0][0].data.numpy()
                # i2 = y_img_mask_r[0][0].data.numpy()
                # fig.add_subplot(121)
                # plt.imshow(i1)
                # fig.add_subplot(122)
                # plt.imshow(i2)
                # plt.show()
                # continue

                x_images = x_img_rgb_.to(device=device, dtype=torch.float32)
                y_masks = y_img_mask_.to(device=device, dtype=torch.float32)

                ###################
                # UNET

                adam_optimizer_unet.zero_grad()

                # predictions
                pred_masks_init = unet_model(x_images)

                # loss
                u_loss = unet_l(pred_masks_init, y_masks)
                total_loss_unet += u_loss.item()

                # gradients
                u_loss.backward()

                # clip large gradients
                nn.utils.clip_grad_value_(unet_model.parameters(), 0.1)

                adam_optimizer_unet.step()

                # convert both original and predicted masks to be [0,255]
                pred_masks_fin = torch.sigmoid(pred_masks_init) > 0.5
                pred_masks_fin = pred_masks_fin.cpu()

                predicted_masks = pred_masks_fin.data.numpy() * 255
                predicted_masks = np.squeeze(predicted_masks.astype(np.uint8), axis=1)

                y_masks = y_img_mask_.data.numpy() * 255
                y_masks = np.squeeze(y_masks.astype(np.uint8), axis=1)

                # calculate Total/Avg F1 for only last 20 iterations to save training time
                if batch_idx in list(range(len(train_set) // cnf["BATCH_SZ"] - 20, len(train_set) // cnf["BATCH_SZ"])):

                    for idx, mask in enumerate(predicted_masks):
                        predicted_polys, _ = extract_polys(mask, return_wkt=False)
                        actual_polys, _ = extract_polys(y_masks[idx], return_wkt=False)

                        e_TP, e_FP, e_FN = calc_scores(actual_polys, predicted_polys)
                        f1_total.append(calc_f1(e_TP, e_FP, e_FN))

                        TP += e_TP
                        FP += e_FP
                        FN += e_FN

                #######
                # END BATCH

        #######
        # END EPOCH

        '''
        RUN VALIDATION
        '''
        with torch.no_grad():
            val_loss_unet, val_f1, val_f1_avg = validate_and_score(validation_set, val_dataloader, unet_model)

        '''
        LOGGING RESULTS
        '''
        logger = {
            "EPOCH:": epoch,
            "Training Loss (UNET):": total_loss_unet,
            "Training F1 (Total):": calc_f1(TP, FP, FN),
            "Training F1 (AVG):": sum(f1_total) / len(f1_total) if not VALIDATION_ONLY else 0,
            "LR:": adam_optimizer_unet.param_groups[0]['lr'],
            "VAL Loss (UNET):": val_loss_unet,
            "VAL F1 (Total):": val_f1,
            "VAL F1 (AVG):": val_f1_avg,

        }

        # construct log string
        example_log_string = "\n"
        for k, v in logger.items():
            example_log_string += k + " " + str(v) + "\n"

        print(example_log_string)

        if VALIDATION_ONLY:
            exit()

        # write log to file
        log_file = open(cnf["CHECKPOINTS_DIR"] + "/log.txt", 'a')
        log_file.write(example_log_string)
        log_file.close()

        lr_scheduler_unet.step(val_loss_unet)

        '''
        SAVE CHECKPOINT
        '''
        # save new checkpoint
        new_state = {
            "epoch": epoch,
            "unet": unet_model.state_dict(),
            "scheduler_unet": lr_scheduler_unet.state_dict(),
            "optimizer_unet": adam_optimizer_unet.state_dict(),

        }

        torch.save(new_state, cnf["CHECKPOINTS_DIR"] + "/CK-" + str(epoch))


def validate_and_score(validation_set, val_dataloader, unet_model, compressor_model):
    global VALIDATION_ONLY

    val_loss_unet, val_loss_fin = 0, 0
    TP, FP, FN = 0, 0, 0
    f1_total = []

    # change model mode to eval
    unet_model.eval()

    # losses
    unet_l = unet_loss().to(device=device)

    for batch_idx, (x_img_rgb_, x_img_sar_, y_img_mask_, y_polys_str, img_orientation) in enumerate(
            tqdm(val_dataloader, desc="Validation")):

        # copy image/mask to GPU
        x_images = x_img_rgb_.to(device=device, dtype=torch.float32)
        y_masks = y_img_mask_.to(device=device, dtype=torch.float32)

        # make prediction
        pred_masks_init = unet_model(x_images)

        # calculate losses
        loss_u = unet_l(pred_masks_init, y_masks)
        val_loss_unet += loss_u.item()

        # convert predicted masks to be [0,255]
        pred_masks_init = torch.sigmoid(pred_masks_init) > 0.5
        pred_masks_init = pred_masks_init.cpu()

        predicted_masks = pred_masks_init.data.numpy() * 255
        predicted_masks = np.squeeze(predicted_masks.astype(np.uint8), axis=1)

        # fig=plt.figure(figsize=(50,10))
        # i1 = np.transpose(x_img_rgb_[0].data.numpy(),[1,2,0])
        # i2 = np.transpose(x_img_sar_[0].data.numpy(),[1,2,0])
        # i3 = y_img_mask_[0][0].data.numpy()
        # i4 = predicted_masks[0]

        # fig.add_subplot(141)
        # plt.imshow(i1)
        # fig.add_subplot(142)
        # plt.imshow(i2)
        # fig.add_subplot(143)
        # plt.imshow(i3)
        # fig.add_subplot(144)
        # plt.imshow(i4)
        # plt.show()

        # calculate Total/Avg F1 for only last 20 iterations to save training time
        if VALIDATION_ONLY or batch_idx in list(range(len(validation_set) - 20, len(validation_set))):
            for idx, mask in enumerate(predicted_masks):
                predicted_polys, _ = extract_polys(mask, img_orientation=img_orientation[idx], return_wkt=False,
                                                   simplify=True, min_area=80.0)
                actual_polys = [loads(p) for p in y_polys_str[idx].split("-")]
                actual_polys = [p for p in actual_polys if p.area >= 80.0]

                e_TP, e_FP, e_FN = calc_scores(actual_polys, predicted_polys)
                f1_total.append(calc_f1(e_TP, e_FP, e_FN))

                TP += e_TP
                FP += e_FP
                FN += e_FN

    val_f1_score = calc_f1(TP, FP, FN)
    val_f1_avg = sum(f1_total) / len(f1_total)

    return val_loss_unet, val_f1_score, val_f1_avg


train_model(restore_epoch_number="")
