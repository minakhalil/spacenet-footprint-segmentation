import rasterio
import numpy as np
from shapely.wkt import loads
import cv2
from shapely.geometry import Polygon
from .config import cnf


def normalize(array, i):
    '''
    normalize bands regarding to min/max values specified in config
    :param array: band 2D-array (C,H,W)
    :param i: Channel index
    :return: normalized array range[0,1]
    '''

    array_min, array_max = cnf["MIN_BAND_VALUE"][i], cnf["MAX_BAND_VALUE"][i]

    return ((array - array_min) / (array_max - array_min))


def read_sar_tiff_image(img_path):
    '''
    Read image bands into numpy array
    :param img_path:
    :return: image(channels,width,height)
    '''

    x_img = rasterio.open(img_path)

    band_count = x_img.count
    x_img_values = [normalize(x_img.read(b), i) for i, b in enumerate(range(1, band_count + 1))]
    x_img_values = np.array(x_img_values)
    return x_img_values


def restore_rgb_value(arr, min_, max_):
    '''
    [min->max] -> [0>255]
    :param arr: values [min->max]
    :param min_:
    :param max_:
    :return: values [0->255]
    '''
    n_arr = 255 * (arr - min_) / (max_ - min_)
    n_arr[n_arr > 255] = 255.0
    n_arr[n_arr < 0] = 0.0
    return n_arr


def transform_sar(img):
    '''
    Transform SAR 4 channels HH,HV,VH,VV to RGB using Pauli transformation
    Apply some smoothing to eliminate the random noise
    :param img: SAR 4 channels
    :return: RGB image
    '''
    hh, hv, vh, vv = img

    r_comp = np.square(hh - vv)
    g_comp = np.square(hh + vh)
    b_comp = np.square(hh + vv)

    mins = [0, 0, 0]
    maxs = [0.25, 1.8, 1.8]

    r_comp = restore_rgb_value(r_comp, mins[0], maxs[0])
    g_comp = restore_rgb_value(g_comp, mins[1], maxs[1])
    b_comp = restore_rgb_value(b_comp, mins[2], maxs[2])

    sc_image = np.array([r_comp, g_comp, b_comp])
    sc_image = np.transpose(sc_image, [1, 2, 0])
    sc_image = np.uint8(sc_image)

    sc_image_smoothed = cv2.fastNlMeansDenoisingColored(sc_image, None, 8, 10, 1, 5)

    sc_image_smoothed = np.transpose(sc_image_smoothed, [2, 0, 1])

    return sc_image_smoothed / 255



def construct_mask_image(df, image_id, image_size):
    '''
    Reads ground truth polys and construct the binary mask
    :param image_id: image id in the summary file
    :param image_size: train image size
    :return: the constructed binary mask and the actual polys
    '''
    polys = df.get_group(image_id)["PolygonWKT_Pix"].values
    mask_image = np.zeros((image_size, image_size, 1), np.uint8)

    for poly in polys:
        int_coords = lambda x: np.array(x).round().astype(np.int32)
        try:
            poly = loads(poly)
            x, y = int_coords(poly.exterior.coords.xy)
            coords = np.column_stack([x, y])

            cv2.fillPoly(mask_image, pts=[coords], color=(255, 255, 255))
        except:
            continue

    _, mask_image = cv2.threshold(mask_image, 100, 255, cv2.THRESH_BINARY)
    mask_image = mask_image // 255

    str_merged_poly = "-".join(polys)

    return mask_image, str_merged_poly


def extract_polys(binary_mask, sigmoid_mask=None, img_orientation=0, simplify=True, return_wkt=True, min_area=0):
    '''
    Use provided poly extraction in base line
    also calculates the Confidence based on the sigmoid output
    :param binary_mask: [0,255] image
    :param sigmoid_mask: [0.5,1.0] predicted mask
    :param simplify: bbol if need to simplify with tolerance 0.5
    :return: polys and confidence list
    '''

    # return rotated image to real world dimensions
    if img_orientation == 1:
        binary_mask = np.flip(binary_mask)
        sigmoid_mask = np.flip(sigmoid_mask)

    contours, _ = cv2.findContours(binary_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_L1)
    polys_init = []

    for con in contours:

        try:
            polys_init.append(Polygon(np.squeeze(con)))
        except:
            continue

    polys, confs = [], []
    for p in polys_init:

        if not p.is_valid:
            p = p.buffer(0)

        if simplify:
            poly = p.simplify(tolerance=0.5)
        else:
            poly = p

        if poly.area < min_area:
            continue

        if cnf["CALCULATE_CONFIDENCE"] and sigmoid_mask is not None:
            int_coords = lambda x: np.array(x).round().astype(np.int32)
            blank_image = np.zeros_like(binary_mask)
            x, y = int_coords(poly.exterior.coords.xy)
            coords = np.column_stack([x, y])

            cv2.fillPoly(blank_image, pts=[coords], color=(255))
            indicies_y, indicies_x = np.where(blank_image == 255)
            values = [sigmoid_mask[indicies_y[i]][indicies_x[i]] for i in range(len(indicies_y)) if
                      sigmoid_mask[indicies_y[i]][indicies_x[i]] > 0.5]

            values = [0.5] if len(values) == 0 else values

            conf = sum(values) / len(values)
            conf = (conf * 2.0) - 1.0

            confs.append(conf)
        else:
            confs.append(0.5)

        if return_wkt:
            polys.append(poly.wkt)
        else:
            polys.append(poly)

    return polys, confs


def calc_f1(TP, FP, FN):
    '''
    calculate percision,recall then F1-score
    :param TP: True positives
    :param FP: False positives
    :param FN: False negatives
    :return: F1-score
    '''
    percision = TP / (TP + FP) if (TP + FP) else 0
    recall = TP / (TP + FN) if (TP + FN) else 0

    f1_score = 2 * percision * recall / (percision + recall) if (percision + recall) else 0
    return f1_score * 100


def calc_scores(truth_polys, predicted_polys):
    '''
    Takes the groud truth and predicted list of Polygon and calculate TP, FP, FN for an example
    :param truth_polys: ground truth poly
    :param predicted_polys: predicted polys
    :return: TP, FP, FN depends on IOU mentioned criteria
    '''
    TP, FP, FN = 0, 0, 0
    matched_truth_ids = []

    for p_poly in predicted_polys:

        if not p_poly.is_valid:
            p_poly = p_poly.buffer(0)

        IOU_score = 0
        for t_idx, t_poly in enumerate(truth_polys):

            if t_poly.is_valid and p_poly.is_valid and p_poly.intersects(t_poly):

                IOU_score = max(p_poly.intersection(t_poly).area / p_poly.union(t_poly).area, IOU_score)

                if IOU_score >= 0.5 and t_idx not in matched_truth_ids:
                    TP += 1
                    matched_truth_ids.append(t_idx)
                    break

        if IOU_score < 0.5:
            FP += 1

    FN = len(truth_polys) - len(matched_truth_ids)

    return TP, FP, FN
