cnf = {}

'''
TRAIN DATASET PARAMS
'''
cnf["SAR_DIR"] = "drive/My Drive/sp6/train/AOI_11_Rotterdam/SAR-Intensity"
cnf["SAR_IMAGE_PREFIX"] = "SN6_Train_AOI_11_Rotterdam_SAR-Intensity_"

cnf["RGB_DIR"] = "drive/My Drive/sp6/train/AOI_11_Rotterdam/PS-RGB"
cnf["RGB_IMAGE_PREFIX"] = "SN6_Train_AOI_11_Rotterdam_PS-RGB_"

cnf["SUMMARY_DIR"] = "drive/My Drive/sp6/train/AOI_11_Rotterdam/SummaryData/SN6_Train_AOI_11_Rotterdam_Buildings.csv"
cnf["ORIENTATION_DIR"] = "drive/My Drive/sp6/train/AOI_11_Rotterdam/SummaryData/SAR_orientations.txt"

'''
PREPROCESSING PARAMS
'''
cnf["SEED"] = 42
cnf["VALIDATION_RATIO"] = 0.15
cnf["SINGLE_IMAGE_SPLITS"] = 16

cnf["TEMP_DIR"] = "drive/My Drive/sp6-code6/tmp"
cnf["USE_AUGMENTATION"] = False
cnf["AUGMENTATION_FACTOR"] = 1

# SAR range
cnf["MIN_BAND_VALUE"] = [0., 0., 0., 0.]
cnf["MAX_BAND_VALUE"] = [91.3166, 91.9715, 91.6547, 91.9873]
# RGGB range
cnf["MIN_RGB_VALUE"] = [0., 0., 0.]
cnf["MAX_RGB_VALUE"] = [255., 255., 255.]

'''
TRAINING PARAMS
'''
cnf["BATCH_SZ"] = 8
cnf["USE_GPU"] = True
cnf["LEARNING_RATE"] = 1e-4
cnf["SCHEDULER_PATIENCE"] = 26
cnf["EPOCHS"] = 100
cnf['N_WORKERS'] = 15
cnf['VALIDATION_ONLY'] = False

cnf["CHECKPOINTS_DIR"] = "drive/My Drive/sp6-code6/ck2"
cnf["vgg19_pretrained"] = "drive/My Drive/sp6-code6/pretrained/vgg19.pth"

'''
PREDICTION PARAMS
'''
cnf["SAR_DIR_TEST"] = "drive/My Drive/sp6/test2/SAR-Intensity"
cnf["SAR_IMAGE_PREFIX_TEST"] = "SN6_Test_Public_AOI_11_Rotterdam_SAR-Intensity_"

cnf["SUBMISSION_FILE"] = "drive/My Drive/sp6-code4/submission/submission.csv"
cnf["CALCULATE_CONFIDENCE"] = False  # consumes lot of time if enabled
